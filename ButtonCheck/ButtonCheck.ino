//Written by T1_COMMS_A ENGG200/300/600
//CONTRIBUTORS: TB

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int buttonPin1 = 3;   //the pin of second button
const int buttonPin2 = 7;   //the pin of the third button -> a calling function
const int decButton = 4;  //decrements
const int ledPin1 = 9;
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // backlight on
const int ledPin =  13;      // the number of the LED pin

int bl = 1;


int count = 0;
int toggle = 1;

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int buttonState1 = 0;
int buttonState2 = 0;
int decButtonState = 0;

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin1, OUTPUT);
  // initialize the pushbutton pin as an input
  lcd.begin(16,2);
  lcd.print("Let's begin");
  delay(2000);
  pinMode(buttonPin, INPUT);
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(decButton, INPUT);
  
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  decButtonState = digitalRead(decButton);
  
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) { //HIGH BUTTON STATE MEANS PRESSED
    // turn LED on:
    //digitalWrite(LED_BUILTIN, HIGH);
    count++;
    counter(count);
  //} else {
    // turn LED off:
    //digitalWrite(LED_BUILTIN, LOW);
  }

  if (decButtonState == HIGH) {
    count--;
    counter(count);
  }
  
  if (count%10 == 0) {
    digitalWrite(ledPin1, HIGH);
  } else {
    digitalWrite(ledPin1, LOW);
  }
   
  if(buttonState1 == HIGH && toggle > 0) {
    toggle *= -1;
  }

  if(buttonState2 == HIGH && toggle < 0) {
    toggle *= -1;
  }
  
  if (toggle > 0){
    digitalWrite(ledPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
  }
  
}


void counter(int x) {
  lcd.setCursor(0,1);
  lcd.print("Count: ");
  lcd.setCursor(7,1);
  lcd.print(x);
  delay(250);
  
}
