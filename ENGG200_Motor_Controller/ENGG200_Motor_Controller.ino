#include <Servo.h>  //This comes with the Arduino
#define outputA 7 //white
#define outputB 8 //blue

int MotorPinArm = 11;
int MotorOnPinArm = 5;

int MotorPinRoller = 12;
int MotorOnPinRoller = 6;

Servo MotorArm;
Servo MotorRoller;

int counter = 0;
int aState;
int aLastState;

const int buttonPinExtend = 2;
const int buttonPinStop = 3;
const int buttonPinRetract = 4;

int buttonStateExtend = 0;         // variable for reading the pushbutton status
int buttonStateStop = 0;
int buttonStateRetract = 0;

int MotorMaxForward = 2000;  //this might not actually be forward
int MotorMaxReverse = 1000;    //this might not actually be reverse
int MotorStop = 1500;          //this is definitely stop (if the Motor is calibrated right)
unsigned long nextStep;
unsigned long lastStep;

int state = 0;

void setup() {
  pinMode(MotorOnPinArm, OUTPUT);
  MotorArm.attach(MotorPinArm, 1000, 2000);
  MotorArm.writeMicroseconds(MotorStop);
  pinMode(MotorOnPinRoller, OUTPUT);
  MotorRoller.attach(MotorPinRoller, 1000, 2000);
  MotorRoller.writeMicroseconds(MotorStop);

  lastStep = millis();  // where we are
  nextStep = lastStep + 1000;

  pinMode(buttonPinExtend, INPUT);
  pinMode(buttonPinStop, INPUT);
  pinMode(buttonPinRetract, INPUT);

  pinMode (outputA, INPUT);
  pinMode (outputB, INPUT);

  Serial.begin(9600);
  // Reads the initial state of the outputA
  aLastState = digitalRead(outputA);
}

void loop() {
  buttonStateExtend = digitalRead(buttonPinExtend);
  buttonStateStop = digitalRead(buttonPinStop);
  buttonStateRetract = digitalRead(buttonPinRetract);

  unsigned long inStep;  // how long we are in the current step

  inStep = (millis() - lastStep) / 2;
  if (inStep > 1000) {
    inStep = 1000;  // don't go beyond the step time
  }
  switch (state) {

    case 0: //both motors off
      digitalWrite(MotorOnPinArm, LOW);
      digitalWrite(MotorOnPinRoller, LOW);
      MotorArm.writeMicroseconds(MotorStop);
      MotorRoller.writeMicroseconds(MotorStop);
      break;

    case 1: //arm extend && rollers on
      digitalWrite(MotorOnPinArm, LOW);
      digitalWrite(MotorOnPinRoller, LOW);

      aState = digitalRead(outputA); // Reads the "current" state of the outputA
      // If the previous and the current state of the outputA are different, that means a Pulse has occured
      if (aState != aLastState) {
        // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
        if (digitalRead(outputB) != aState) {
          counter ++;
        } else {
          counter --;
        }
        Serial.print("Position: ");
        Serial.println(counter);
      }
      aLastState = aState; // Updates the previous state of the outputA with the current state

      if (counter == -200)
        state = 3;
      

      MotorArm.writeMicroseconds(MotorStop + inStep);
      MotorRoller.writeMicroseconds(MotorStop + inStep);
      break;

    case 2: //arm off && rollers on
      digitalWrite(MotorOnPinArm, LOW);
      digitalWrite(MotorOnPinRoller, LOW);
      MotorArm.writeMicroseconds(MotorStop);
      MotorRoller.writeMicroseconds(MotorMaxForward);
      break;

    case 3: //arm retract && roller off
      digitalWrite(MotorOnPinArm, LOW);
      digitalWrite(MotorOnPinRoller, LOW);

      aState = digitalRead(outputA); // Reads the "current" state of the outputA
      // If the previous and the current state of the outputA are different, that means a Pulse has occured
      if (aState != aLastState) {
        // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
        if (digitalRead(outputB) != aState) {
          counter ++;
        } else {
          counter --;
        }
        Serial.print("Position: ");
        Serial.println(counter);
      }
      aLastState = aState; // Updates the previous state of the outputA with the current state

      if (counter == 0) 
        state = 0;
      
      MotorArm.writeMicroseconds(MotorStop - inStep);
      MotorRoller.writeMicroseconds(MotorStop);
      break;
  }

  if (buttonStateExtend == HIGH) { //HIGH BUTTON STATE MEANS PRESSED
    state = 1;
  }
  if (buttonStateStop == HIGH) { //HIGH BUTTON STATE MEANS PRESSED
    state = 2;
  }
  if (buttonStateRetract == HIGH) { //HIGH BUTTON STATE MEANS PRESSED
    state = 3;
  }
}
