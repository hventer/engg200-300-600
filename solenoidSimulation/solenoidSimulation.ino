#include <I2CIO.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

#include <Servo.h>

//STATES OF GRABBING
//state 1: magnet off SG1
//state 2: magnet on SG2

//STATES OF ARM EXTENSION
//state 1: retracted SAE1
//state 2: extending SAE2
//state 3: extended SAE3
//state 4: retracting SAE4

//STATES OF THE ARM GRABBING
//state 1: magnet off but arm is retracted 
//state 2: magnets off and arm is extending -> magnet is off always when extending
//state 3: magnet off but arm is extended
//state 4: magnet on and arm is extended
//state 5: magnet on but arm is retracting -> magnet is always on when retracting
//state 6: magnet on but arm is retracted

/*
 * States combined.
 * state 1: SG1 SAE1
 * state 2: SG1 SAE2
 * state 3: SG1 SAE3
 * state 4: SG2 SAE3
 * state 5: SG2 SAE4
 * state 6: SG2 SAE1
 */

//pin locations
const int extendLedPin = 2;
const int retractLedPin = 3;
const int magnetLedPin = 4;

const int extendToggler = 9;
const int retractToggler = 10;
const int magnetToggler = 11;

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
int bl = 1;

int Motor_max_forward = 2000;  //this might not actually be forward
int Motor_max_reverse = 1000;    //this might not actually be reverse
int Motor_stop = 1500;          //this is definitely stop (if the Motor is calibrated right)
unsigned long next_step;
unsigned long last_step;
int state = 0;

int magnetToggleState = 0;
int extendToggleState = 0;
int retractToggleState = 0;

bool extend = false;
bool retract = false;
bool magnet = false;

void setup() {
  pinMode(extendLedPin, OUTPUT);
  pinMode(retractLedPin, OUTPUT);
  pinMode(magnetLedPin, OUTPUT);
  
  pinMode(magnetToggler, INPUT);
  pinMode(extendToggler, INPUT);
  pinMode(retractToggler, INPUT);
  
  lcd.begin(16,2);
  lcd.print("SOLENOID GRABBER");
  delay(500);
}

//lets assume that this program is needed to get 3 cans and we get one can at a time.
int cans = 0;

//lets assume that it takes 5 seconds to do each action (extend and retract


void execute(int numberCans) { //extend. pick up can. retract is one execute
  if(numberCans != 0) {
    //Start by extending hand
    extend = true;
    armExtend();
    delay(5000);
    extend = false;
    armExtend();
    

    //magnet on
    magnet = true;
    armGrab();

    //retract
    retract = true;
    armRetract();
    delay(5000);

    magnet == false;
    armGrab();
  }
}

void loop() {
  //initialising state of buttons
  //HIGH state means button being pressed
  //LOW state means not pressed
  magnetToggleState = digitalRead(magnetToggler);
  extendToggleState = digitalRead(extendToggler);
  retractToggleState = digitalRead(retractToggler);
  
  if(magnetToggleState == HIGH){
    //magnet = !magnet;
    //delay(500);
    cans = 3;
  }

  if(extendToggleState == HIGH) {
    //extend = !extend;
    //delay(500);
    cans = 1;
  }

  if(retractToggleState == HIGH) {
    //retract =! retract;
    //delay(500);
    cans = 2;
  }
  
  
  //----- Functions below are just active listening functions, listening to global vars------
  //if magnet is toggled on, no need for armGrab dropCan functions

  /*
  if(magnet == true) {
    armGrab();
  } else {
    dropCan();
  }

  armExtend();
  armRetract();
  */

  counter(cans);
  
  if(cans != 0) {
    //Start by extending hand
    extend = true;
    armExtend();
    delay(5000);
    extend = false;
    armExtend();
    

    //magnet on
    magnet = true;
    armGrab();

    //retract
    retract = true;
    armRetract();
    delay(5000);
    retract = false;
    armRetract();

    magnet = false;
    armGrab();

    cans--;
  }

  counter(cans);
  
}

void counter(int x) {
  lcd.setCursor(0,1);
  lcd.print("  Cans left: ");
  lcd.setCursor(12,1);
  lcd.print(x);
  delay(250);
}

void armGrab() {
    if(magnet == true) {
    digitalWrite(magnetLedPin, HIGH);
  } else {
    digitalWrite(magnetLedPin, LOW);
  }//digitalWrite(magnetLedPin, HIGH);
}

void dropCan() {
   digitalWrite(magnetLedPin, LOW);
}

void armExtend() {
  if(extend == true) {
    digitalWrite(extendLedPin, HIGH);
  } else {
    digitalWrite(extendLedPin, LOW);
  }
}

void armRetract() {
  if(retract == true) {
    digitalWrite(retractLedPin, HIGH);
  } else {
    digitalWrite(retractLedPin, LOW);
  }
}
