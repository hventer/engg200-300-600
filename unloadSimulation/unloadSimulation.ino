#include <I2CIO.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

#include <Servo.h>

//pin locations
//Two LED lights, One for door and one for solenoid lock
//One more to simulate can leaving.
const int doorServoLedPin = 5;
const int lockerLedPin = 6;
const int counterTrip = 7;

//One button to unload n cans 
const int unloadToggle = 8;

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
int bl = 1;

int unloadState = 0;

bool unload = false; //default state of unloading is FALSE. 
bool door = false; //default state of door FALSE is closed and TRUE is open. 
bool lock = true; //default state of the lock is locked.

//Let's assume that unload n cans takes n*0.5 second for testing sakes
//Thus for each second, the counter simulates an increment/decrement where the button is pressed
int cans = 10; //input amount of cans here.
int count = 0;
//One button to unload n cans 
//Two LED lights, One for door and one for solenoid lock

void setup() {
  pinMode(doorServoLedPin, OUTPUT);
  pinMode(lockerLedPin, OUTPUT);
  pinMode(counterTrip, OUTPUT);
  
  pinMode(unloadToggle, INPUT);

  lcd.begin(16,2);
  lcd.print("CAN UNLOAD");
  delay(500);
}

void loop() {
  unloadState = digitalRead(unloadToggle);
  
  if(unloadState == HIGH) { //button toggles. It will return back to once execution is finished.
    unload = !unload;
    delay(1000);
  }

  if(unload == true) {
    if(count != cans) {
      counter(count);
    }

    //This checks if the door is opened yet or not
    if(lock != false && door != true) {
      lock = false; //we want the lock to unlock first -> so far this is unnecessary
      unlock(true);
      delay(1000); //assume that a solenoid takes .5 seconds to open
    
      door = true; //door being true means opening door
      actionDoor(true); //door is being opened
      delay(5000); // assume that it takes 1 second to open
      actionDoor(false); //door opened
      counter(count);
    }
  
    //If the door is already opened and number of unloaded cans != initial amount of cans
    //can counting method
    if(lock == false && door == true && count != cans) {
      /*digitalWrite(counterTrip, HIGH);
      delay(500);  
      count++;
      digitalWrite(counterTrip, LOW);
      counter(count);
      */
      canCount();
    }

    //if door is opened and number of unloaded cans == initial amount of cans
    if(lock == false && door == true && count >= cans) {
      delay(5000); // make sure that there are any other cans can come out
      actionDoor(true); //door is being closed
      delay(5000);  
      actionDoor(false); //door is closed
      door = false;
      
      unlock(false); //locking the solenoid
      delay(500);
      lock = true;

      unload = false; //that means this can unloading is complete
      counter(count);
      count = 0; //for the sake of resetting the amount of cans
    } 

  }
}

void canCount() {
  digitalWrite(counterTrip, HIGH);
  delay(500);  //we are assuming a can goes over button/sensor every 0.5 seconds
  /*
   * if(sensor/buttonState == high) {
   *  count++;
   * }
   * counter(count);
   * above code is the actual counting cans.
   * we have to make sure that the button is debounced.
   */
  count++;
  digitalWrite(counterTrip, LOW);
  counter(count);
}

void actionDoor(bool check) {
  if(check == true) {
    digitalWrite(doorServoLedPin, HIGH);
  } else {
    digitalWrite(doorServoLedPin, LOW);
  }
  /*
   * void openDoor(bool check) {
   *  if(check == true) {
   *      myServo.write(180); //opens door 
   *  }
   * }
   * 
   * void closeDoor(bool check) {
   *  if(check == true) {
   *    myServo.write(0); closes door 
   *  }
   * }
   *    
   */
}

void unlock(bool check) {
  if(check == true) {
    digitalWrite(lockerLedPin, HIGH);
    //digitalWrite(solenoidLock, HIGH);
  } else {
    digitalWrite(lockerLedPin, LOW);
    //digitalWrite(solenoidLock, LOW);
  }
  
}

void counter(int x) {
  lcd.setCursor(0,1);
  lcd.print("Cans unloaded: ");
  lcd.setCursor(14,1);
  lcd.print(x);
  delay(250);
}
